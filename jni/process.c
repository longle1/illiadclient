#include <jni.h>
#include "fftw3/include/fftw3.h"
#include <math.h>
#include "window.h"
#include <android/log.h>

#define alpha_updown 			0.05
#define slow_scale 				0.5
#define floor_up 				1+alpha_updown
#define floor_up_slow			1+slow_scale*alpha_updown
#define floor_down				1-alpha_updown
#define indicator_countdown		2

#define wiener_scale			0.01
#define wiener_min				0.1

#define det_thresh_scale		300

#define fs						8000
#define whistleBW				16


JNIEXPORT void JNICALL Java_com_longle1_spectrogram_Spectrogram_process2
	(JNIEnv *env, jclass obj, jobject inbuf, jobject outbuf, jint N, jobject thresh, jobject indict)
{
	int i;
	double wts, muval;
	int binmin, binmax,cnt;
	char buf[128];

	short* inBuf = (short*)(*env)->GetDirectBufferAddress(env,inbuf);
	double* outBuf = (double*)(*env)->GetDirectBufferAddress(env,outbuf);

	double* floor_thresh = (double*)(*env)->GetDirectBufferAddress(env,thresh);
	char* indicator_last = (char*)(*env)->GetDirectBufferAddress(env,indict);

	fftw_complex *out;
	fftw_plan p;
	double *in;

	in = (double*) fftw_malloc(sizeof(double) * 2 * N);
	out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N+1));

	p = fftw_plan_dft_r2c_1d(2*N, in, out, FFTW_ESTIMATE);

	for(i=0; i<N; i++) {
		// pack into short assuming little endian, and convert to float
		in[i] = (double)inBuf[i] / 128;
		// window
		in[i] = window[i]*in[i];
	}
	// zero-pad
	for(i=N; i<2*N; i++) {
		in[i] = 0;
	}

	fftw_execute(p);

	double maxval = 0;
	int maxidx = 0;
	for(i=0; i<N; i++) {
		// compute energy
		outBuf[i] = out[i][0]*out[i][0] + out[i][1]*out[i][1];

		// noise floor tracking
        if(outBuf[i] > floor_thresh[i]) {
        	indicator_last[i]--;
    		floor_thresh[i] *= (indicator_last[i]<0) ? floor_up_slow : floor_up;
        } else { // background noise is highly probable
            indicator_last[i] = indicator_countdown;
            floor_thresh[i] *= floor_down;
		}

        wts = fmax(fmin(1,wiener_scale*outBuf[i]/floor_thresh[i]),wiener_min);
        outBuf[i] = outBuf[i] * wts;

        // compute max over frame
        if(outBuf[i] > maxval) {
        	maxval = outBuf[i];
        	maxidx = i;
        }
	}

	binmin = floor(500.0f/fs*2*N);
	binmax = floor(4000.0f/fs*2*N)-1;
    if(maxidx>=binmin && maxidx<=binmax) {
    	cnt = 0;
    	muval = 0;
    	for(i=binmin;i<maxidx-whistleBW/2;i++) {
    		if(outBuf[i] > floor_thresh[i]) {
    			cnt++;
    			muval += outBuf[i];
    		}
    	}
    	for(i=maxidx+whistleBW/2;i<binmax;i++) {
    		if(outBuf[i] > floor_thresh[i]) {
    			cnt++;
    			muval += outBuf[i];
    		}
    	}
    	if(cnt == 0) {
    		// all bins have signal less than noise floor. best guess is noise floor...
    		muval = floor_thresh[maxidx];
    	} else {
    		muval /= (float)cnt;
    	}

		for(i=0;i<N;i++) {
			outBuf[i] = (maxval/muval > det_thresh_scale) ? log10(outBuf[i]) / 12.0f + 0.5 : 0;
			outBuf[i] = (outBuf[i]<0) ? 0 : outBuf[i];
			outBuf[i] = (outBuf[i]>1) ? 1 : outBuf[i];
    	}
		sprintf(buf,"%f",maxval/muval);
    } else {
    	for(i=0;i<N;i++) {
    		outBuf[i] = 0;
    	}
    	sprintf(buf,"(%d,%d): outside 500Hz-4kHz", binmin, binmax);
    }

	__android_log_write(ANDROID_LOG_INFO, "process2", buf);

	// rescale for displaying
	/*
	for(i=0;i<N;i++) {
		if(outBuf[i] > det_thresh_scale*floor_thresh[i])
			outBuf[i] = log10(outBuf[i]) / 12.0f + 0.5;
		else
			outBuf[i] = 0;
		outBuf[i] = (outBuf[i]<0) ? 0 : outBuf[i];
		outBuf[i] = (outBuf[i]>1) ? 1 : outBuf[i];
	}
	*/

	fftw_destroy_plan(p);
	fftw_free(in);
	fftw_free(out);
}

JNIEXPORT void JNICALL Java_com_longle1_spectrogram_ProcessService_process
	(JNIEnv *env, jclass obj, jshortArray jinbuf, jdoubleArray joutbuf, jint N)
{
	int i;
	short* inBuf = (short*)(*env)->GetShortArrayElements(env,jinbuf,0);
	double* outBuf = (double*)(*env)->GetDoubleArrayElements(env,joutbuf,0);
	double *in;
	fftw_complex *out;
	fftw_plan p;


	in = (double*) fftw_malloc(sizeof(double) * 2 * N);
	for(i=0; i<2*N; i++) {
		// pack into short assuming little endian, and convert to float
		in[i] = (double)inBuf[i] / SHRT_MAX;
		// window
		in[i] = window[i]*in[i];
	}
	// zero-pad
	/*
	for(i=N; i<2*N; i++) {
		in[i] = 0;
	}
	*/
	
	out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (N+1));

	p = fftw_plan_dft_r2c_1d(2*N, in, out, FFTW_ESTIMATE);
	//p = fftw_plan_dft_1d(2*N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_execute(p);

	for(i=0; i<N; i++) {
		// compute energy
		outBuf[i] = sqrt(out[i][0]*out[i][0] + out[i][1]*out[i][1]);
		//outBuf[i] = log10(outBuf[i]) / 12.0f + 0.5;
		//outBuf[i] = (outBuf[i]<0) ? 0 : outBuf[i];
		//outBuf[i] = (outBuf[i]>1) ? 1 : outBuf[i];
	}

	(*env)->ReleaseShortArrayElements(env, jinbuf, inBuf, 0);
	(*env)->ReleaseDoubleArrayElements(env, joutbuf, outBuf, 0);

	fftw_destroy_plan(p);
	fftw_free(in);
	fftw_free(out);
}

