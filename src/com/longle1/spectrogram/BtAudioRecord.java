package com.longle1.spectrogram;

import java.io.IOException;
import java.io.InputStream;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioRecord;
import android.util.Log;

public class BtAudioRecord extends AudioRecord {
	Context mContext;
	BluetoothAdapter mBluetoothAdapter;
	private final BroadcastReceiver mReceiverFound;
	private final BroadcastReceiver mReceiverStateChanged;
	BluetoothSocket mSocket = null;
	
	final int PKT_LEN = 10;
	
	BtAudioRecord(int audioSource, int sampleRateInHz, int channelConfig, int audioFormat, int bufferSizeInBytes, Context context){
		super(audioSource, 16000, channelConfig, audioFormat, bufferSizeInBytes); // any parameters will work just fine.
		mContext = context;
		
	    mReceiverFound = new BroadcastReceiver() {
			public void onReceive(Context context, Intent intent) {
	            String action = intent.getAction();
	            // When discovery finds a device
	            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
	                // Get the BluetoothDevice object from the Intent
	                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	                // Add the name and address to an array adapter to show in a ListView
	                if (device!= null && device.getName().contains("BlueCreation")){
						Log.i("BtAudioRecord", "Found a MotionNet "+device.getName()+" at "+device.getAddress());
						connect(device);
					}
	            }
	        }
	    };
	    // Register the BroadcastReceiver
	 	mContext.registerReceiver(mReceiverFound, new IntentFilter(BluetoothDevice.ACTION_FOUND)); // Don't forget to unregister when done
	 	
	 	mReceiverStateChanged = new BroadcastReceiver() {
	 	    @Override
	 	    public void onReceive(Context context, Intent intent) {
	 	        final String action = intent.getAction();

	 	        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
	 	            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
	 	                                                 BluetoothAdapter.ERROR);
	 	            switch (state) {
	 	            case BluetoothAdapter.STATE_OFF:
	 	                Log.i("BtAudioRecord", "Bluetooth linkOffset");
	 	                try {
		 	      			if (mSocket != null)
		 	      				mSocket.close();
		 	              } catch (IOException closeException) { }
		 	      		mSocket = null;
		 	      		Log.i("BtAudioRecord", "socket closed");
	 	                break;
	 	            case BluetoothAdapter.STATE_TURNING_OFF:
	 	                Log.i("BtAudioRecord", "Turning Bluetooth linkOffset...");
	 	                break;
	 	            case BluetoothAdapter.STATE_ON:
	 	                Log.i("BtAudioRecord", "Bluetooth on");
		 	            // wait for BluetoothAdapter.STATE_ON before startDiscovery,
		 	       		// otherwise discovery will fail 
		 	       		boolean status = mBluetoothAdapter.startDiscovery();
		 	       		Log.i("BtAudioRecord", "startDiscovery status "+ status);
	 	                break;
	 	            case BluetoothAdapter.STATE_TURNING_ON:
	 	                Log.i("BtAudioRecord", "Turning Bluetooth on...");
	 	                break;
	 	            }
	 	        }
	 	    }
	 	};
	 	// Register for broadcasts on BluetoothAdapter state change
	 	mContext.registerReceiver(mReceiverStateChanged, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
	}
	
	public void startRecording() throws IllegalStateException{
		// check if Bluetooth is supported and enable it 
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
			Log.e("BtAudioRecord", "Device does not support Bluetooth");
			throw new IllegalStateException();
		}
		if (!mBluetoothAdapter.isEnabled()) {
			mBluetoothAdapter.enable();
            Log.i("BtAudioRecord", "Bluetooth switched ON");
		}
		
		for (int k = 0; k < 10; k++){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if (mSocket != null){
				return;
			}
		}
		// timeout
		throw new IllegalStateException();
	}
	
	public int read(ByteBuffer audioBuffer, int sizeInBytes){
		// Assert: valid socket
		InputStream mInStream;
		
		try {
			mInStream = mSocket.getInputStream();
        } catch (IOException e) {
        	Log.e("BtAudioRecord", e.toString());
        	return -1;
        }

        // Read from the InputStream
		int nread = 0;
		audioBuffer.position(0); // reset buffer position
		while(nread <= sizeInBytes - PKT_LEN){
			int status = decodePkt(mInStream, audioBuffer);
			if (status == 0){// success
				nread += PKT_LEN;
			} else if(status == -1){ // invalid sample, nothing change with the buffer
				continue;
			} else if (status == -2){ // input stream read fails
				break;
			}
		}
		// Padding if needed
		try{
			int lastLoc = nread-1;
			byte lowByte = audioBuffer.get(lastLoc-1);
			byte highByte = audioBuffer.get(lastLoc);
			
			for (int k = 0; k < sizeInBytes-nread; k++){
				audioBuffer.put(lowByte);
				audioBuffer.put(highByte);
				nread+=2;
			}
		} catch(IndexOutOfBoundsException e){
			Log.e("BtAudioRecord", e.toString());
		}
		
        return nread;
	}
	
	public void stop() throws IllegalStateException{
		// intentionally empty
	}
	
	public void release(){
		if (mBluetoothAdapter.isEnabled()) {
			mBluetoothAdapter.disable();
			Log.i("BtAudioRecord", "Bluetooth switched OFF");
		}
		
		mContext.unregisterReceiver(mReceiverFound);
		mContext.unregisterReceiver(mReceiverStateChanged);
		
		try {
			if (mSocket != null)
				mSocket.close();
        } catch (IOException closeException) { }
		mSocket = null;
		
		super.release();
	}
	
	private int decodePkt(InputStream mInStream, ByteBuffer bb){
		final byte DLE = 0x10;
		final byte SOH = 0x01;
		final byte  EOT = 0x04;
		
		byte[] cData = new byte[1]; // current data
		byte[] pData = new byte[1]; // past data
		byte[] packet = new byte[256];
		int dataCount = 0;
				
		// find the start of packet
		while (((pData[0] != DLE) || (cData[0] != SOH))){
			pData[0] = cData[0];
			try {
				mInStream.read(cData);
			} catch (IOException e) {
				Log.e("BtAudioRecord", e.toString());
				return -2;
			}
			if ((pData[0] == DLE) && (cData[0] == DLE)){
				pData[0] = cData[0];
				try {
					mInStream.read(cData);
				} catch (IOException e) {
					Log.e("BtAudioRecord", e.toString());
					return -2;
				}
			}
		}
		// find the end of packet
		while (((pData[0] != DLE) || (cData[0] != EOT))){	
			pData[0] = cData[0];
			try {
				mInStream.read(cData);
			} catch (IOException e) {
				Log.e("BtAudioRecord", e.toString());
				return -2;
			}
		
			if (cData[0] != DLE)
				packet[(dataCount++)%256] = cData[0];
			else
			{
				pData[0] = cData[0];
				try {
					mInStream.read(cData);
				} catch (IOException e) {
					Log.e("BtAudioRecord", e.toString());
					return -2;
				}
				if (cData[0] == DLE){
					packet[(dataCount++)%256] = cData[0];
				}
			}
		}
		// return the decoded packet, if valid
		if(dataCount == PKT_LEN){
			for(int i = 0; i<PKT_LEN/2; i++){
				// format a sample
				ByteBuffer sample = ByteBuffer.allocate(2);
				sample.order(ByteOrder.nativeOrder());
				sample.put(packet[i*2]);
				sample.put(packet[i*2+1]);
				short shortVal = sample.getShort(0);
				try{
					bb.putShort((short) ((shortVal-1024) << 4));
				} catch (BufferOverflowException e){
					Log.e("BtAudioRecord", e.toString());
				}
			}
			return 0;
		}
		else
			return -1;
	}
	
	private void connect(BluetoothDevice device){
		// Cancel discovery because it will slow down the connection
        mBluetoothAdapter.cancelDiscovery();
		
		// Get a BluetoothSocket to connect with the given BluetoothDevice
        try {
            // paired device's UUID string. MUST use insecure mode to connect to MotionNet
            mSocket = device.createInsecureRfcommSocketToServiceRecord(device.getUuids()[0].getUuid());
        } catch (IOException e) {
        	mBluetoothAdapter.startDiscovery(); // continue discovery
        	
        	e.printStackTrace();
        	
        	return;
        }
        
        // Connect the device through the socket.
        try {
            // This will block until it succeeds or throws an exception
            mSocket.connect();
        } catch (IOException connectException) {
        	mBluetoothAdapter.startDiscovery(); // continue discovery
        	
        	connectException.printStackTrace();
            // Unable to connect; close the socket and get out
        	try {
    			if (mSocket != null)
    				mSocket.close();
            } catch (IOException closeException) { }
    		mSocket = null;
            
            return;
        }
 
        Log.i("BtAudioRecorder", "socket.connect successful!");
	}
}
