package com.longle1.spectrogram;

import java.util.Date;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;

public class CommunicationManager {
	final int HOLDTIME = 5; // wifi hold time, in minutes
	Context mContext;
	Date wifiStartDate;
	WifiManager wifiManager;
	ConnectivityManager connManager;
	
	public CommunicationManager(Context context){
		mContext = context;
		wifiStartDate = new Date(System.currentTimeMillis()-HOLDTIME*60*1000);
		wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
		connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
	}
	
	public void update(boolean isReady){
		if (wifiManager.isWifiEnabled()){
			if (wifiStartDate.before(new Date(System.currentTimeMillis()-HOLDTIME*60*1000))){
				wifiManager.setWifiEnabled(false);
			}
		}else{ 
			if (isReady) {
				wifiManager.setWifiEnabled(true);
				wifiStartDate = new Date();
			}
		}
	}
	
	public boolean isNetworkReady(){
		return connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
		//return wifiManager.getWifiState()==WifiManager.WIFI_STATE_ENABLED;
	}
}
