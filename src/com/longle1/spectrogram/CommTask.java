package com.longle1.spectrogram;

import org.json.JSONObject;

import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;

public class CommTask extends AsyncTask<Void, Void ,Void>{
	Looper mAsyncHttpLooper;
	Context mBaseContext;
	
	CommunicationManager mCm;
	String mTargetURI;
	RequestParams mRpPost;
	
	JSONObject mPostObj;
	byte[] mWavebb;
	
	public CommTask(Looper asyncHttpLooper,Context baseContext,CommunicationManager cm,String targetURI,RequestParams rpPost,JSONObject postObj){
		mAsyncHttpLooper = asyncHttpLooper;
		mBaseContext = baseContext;
		
		mCm = cm;
		mTargetURI = targetURI;
		mRpPost = rpPost;
		
		mPostObj = postObj;
	}
	
	public CommTask(Looper asyncHttpLooper,Context baseContext,CommunicationManager cm,String targetURI,RequestParams rpPost,byte[] wavebb){
		mAsyncHttpLooper = asyncHttpLooper;
		mBaseContext = baseContext;
		
		mCm = cm;
		mTargetURI = targetURI;
		mRpPost = rpPost;
		
		mWavebb = wavebb;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		while (!mCm.isNetworkReady());
		
		TimedAsyncHttpResponseHandler httpHandler= new TimedAsyncHttpResponseHandler(mAsyncHttpLooper, mBaseContext);
		if (mWavebb != null){
			httpHandler.executePost(mTargetURI+"/gridfs", mRpPost, mWavebb);
		} else if (mPostObj != null){
			httpHandler.executePost(mTargetURI+"/write", mRpPost, mPostObj);
		}
		
		return null;
	}
}
