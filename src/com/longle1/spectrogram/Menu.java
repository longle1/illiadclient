/*******************************************************************************
 * Copyright (c) 2014 University of Illinois at Urbana-Champaign.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Long Le, David Jun, and Douglas Jones - initial API and implementation
 *******************************************************************************/
package com.longle1.spectrogram;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.BufferType;

public class Menu extends Activity {
	Button button;
	EditText edittext1;
	EditText edittext2;
	EditText edittext3;
	EditText edittext4;
	CheckBox checkbox1;
	CheckBox checkbox2;
	CheckBox checkbox3;
	RadioGroup radioGroup1;
	RadioGroup radioGroup2;
	RadioButton radio0;
	RadioButton radio1;
	RadioButton radio2;
	RadioButton radio3;
	SeekBar seekBar1;
	Spinner spinner1;
	
	//BatteryLevelReceiver mBatteryLevelReceiver = new BatteryLevelReceiver();
	boolean start = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		final Context context = this;
		edittext1 = (EditText) findViewById(R.id.editText1);
		button = (Button) findViewById(R.id.button1);
		checkbox1 = (CheckBox) findViewById(R.id.checkBox1);
		checkbox2 = (CheckBox) findViewById(R.id.checkBox2);
		checkbox3 = (CheckBox) findViewById(R.id.checkBox3);
		radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
		radioGroup2 = (RadioGroup) findViewById(R.id.radioGroup2);
		radio0 = (RadioButton) findViewById(R.id.radio0);
		radio1 = (RadioButton) findViewById(R.id.radio1);
		radio2 = (RadioButton) findViewById(R.id.radio2);
		radio3 = (RadioButton) findViewById(R.id.radio3);
		seekBar1 = (SeekBar) findViewById(R.id.seekBar1);
		spinner1 = (Spinner) findViewById(R.id.spinner1);
		
		// Log app start time
		String sdcard = Environment.getExternalStorageDirectory().getPath();
        File dir = new File(sdcard, this.getString(this.getApplicationInfo().labelRes));
        if(!dir.exists()){
        	dir.mkdirs();
        }
        try {
        	File file = new File(dir, "appStartTime.txt");
        	OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
            outputStreamWriter.write(new Date().toString());
            outputStreamWriter.close();
		} catch (IOException  e) {
			e.printStackTrace();
		}
		
        // Setup spinner
        List<String> list = new ArrayList<String>();
		list.add("Internet");
		list.add("Localnet");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, list){
		    public View getView(int position, View convertView, ViewGroup parent) {
		        View v = super.getView(position, convertView, parent);
		        ((TextView) v).setGravity(Gravity.CENTER);
		        ((TextView) v).setTextSize(25);
		        return v;
		    }

		    public View getDropDownView(int position, View convertView, ViewGroup parent) {
		        View v = super.getDropDownView(position, convertView,parent);
		        ((TextView) v).setGravity(Gravity.CENTER);
		        ((TextView) v).setTextSize(25);
		        return v;
		    }
		};
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner1.setAdapter(dataAdapter);
        
		spinner1.setOnItemSelectedListener(new OnItemSelectedListener (){
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				if (parent.getItemAtPosition(pos).toString().compareTo("Internet") == 0){
					edittext1.setText("http://acoustic.ifp.illinois.edu:8956\n"
						+"publicDb\n"+"publicUser\n"+"publicPwd", BufferType.EDITABLE);
				}else{
					edittext1.setText("http://192.168.12.1:8956\n"
						+"publicDb\n"+"publicUser\n"+"publicPwd", BufferType.EDITABLE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				edittext1.setText("http://acoustic.ifp.illinois.edu:8956\n"
					+"publicDb\n"+"publicUser\n"+"publicPwd", BufferType.EDITABLE);
			}
		});
		
		// Setup "generalized" callbacks
		//this.registerReceiver(mBatteryLevelReceiver, new IntentFilter(Intent.ACTION_BATTERY_LOW));
		
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!start){
					// handle conflicting button/state
					if (GlobalState.getGlobalState().getBGstate() == true){
						start = true;
						button.setText(getResources().getString(R.string.stop)); // Change the button label
						return;
					}
					
					start = true;
					button.setText(getResources().getString(R.string.stop)); // Change the button label
					GlobalState.getGlobalState().setBGstate(true);
					GlobalState.getGlobalState().setGUIstate(checkbox1.isChecked());
					
			    	String config = edittext1.getText().toString();
			    	String[] splitted = config.split("\n");
			    	Bundle bundle = new Bundle();
				    bundle.putString("serverURI", splitted[0]);
				    bundle.putString("devName", splitted[2]);
				    bundle.putString("password", splitted[3]);
				    bundle.putString("database", splitted[1]);
				    bundle.putString("androidID", Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));
				    bundle.putInt("thresh", seekBar1.getProgress());
				    String radioButton1 = ((RadioButton)findViewById(radioGroup1.getCheckedRadioButtonId())).getText().toString();
				    String radioButton2 = ((RadioButton)findViewById(radioGroup2.getCheckedRadioButtonId())).getText().toString();
				    if ( radioButton2.equals(radio2.getText().toString()) ){
				    	bundle.putInt("datSrc", 0);// mic
					} else{
						bundle.putInt("datSrc", 1);// bluetooth
					}
				    bundle.putBoolean("isCollect", checkbox2.isChecked()) ;
				    bundle.putBoolean("isSend", checkbox3.isChecked()) ;
				    if ( radioButton1.equals(radio0.getText().toString()) ){
				    	bundle.putBoolean("isEvent", true);
				    } else{
				    	bundle.putBoolean("isEvent", false);
				    }
				    
				    Intent processIntent;
				    processIntent = new Intent(context, ProcessService.class);
					processIntent.putExtra("Menu.Bundle", bundle);
			    	context.startService(processIntent);
			    	
			    	if ( GlobalState.getGlobalState().getGUIstate() ){
			    		Intent intent = new Intent(context, Spectrogram.class);
			    		startActivity(intent);
			    	}
				}
				else{
					// handle conflicting button/state
					if (GlobalState.getGlobalState().getBGstate() == false){
						start = false;
						button.setText(getResources().getString(R.string.start));
						return;
					}
					
					start = false;
					button.setText(getResources().getString(R.string.start));
					GlobalState.getGlobalState().setBGstate(false);
					GlobalState.getGlobalState().setGUIstate(false);
				}                
			}
		});
	}
	
	@Override
	public void onDestroy() {
		GlobalState.getGlobalState().setBGstate(false);
    	super.onDestroy();
    }
	
	static {
    	System.loadLibrary("process");
    }
}
