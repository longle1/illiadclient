package com.longle1.spectrogram;

import java.util.Date;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

public class BatteryLevelMonitor 
{
	Context mContext;
	int batLevel;
	Date batLevelDate;
	
	public BatteryLevelMonitor(Context context){
		mContext = context;
		updateBatState();
	}
	
	public int getBatteryLevel(){
		// the battery state expires after 20 min
		if (batLevelDate.before(new Date(System.currentTimeMillis()-20*60*1000))){
			updateBatState();
		}
		return batLevel;
	}
	
	private void updateBatState(){
		Intent batteryIntent = mContext.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
	    int bmLevel = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
	    int bmScale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
	    batLevel = (int)((double)bmLevel/(double)bmScale*100.0);
	    batLevelDate = new Date();
	    
	    /*
		int batLevel = -1;
		String sdcard = Environment.getExternalStorageDirectory().getPath();
        File dir = new File(sdcard, mContext.getString(mContext.getApplicationInfo().labelRes));
        try {
        	File file = new File(dir, "batteryLevel.txt");
        	OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
        	BufferedReader bufReader = new BufferedReader(new FileReader(file));
        	
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            outputStreamWriter.write(String.valueOf(level));
        	batLevel = Integer.parseInt(bufReader.readLine());
        	
        	outputStreamWriter.close();
        	bufReader.close();
        } catch (IOException  e) {
			e.printStackTrace();
		}
		*/
	}
}