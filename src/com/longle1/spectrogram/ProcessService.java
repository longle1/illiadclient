/*******************************************************************************
 * Copyright (c) 2014 University of Illinois at Urbana-Champaign.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Long Le, David Jun, and Douglas Jones - initial API and implementation
 *******************************************************************************/
package com.longle1.spectrogram;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.longle1.spectrogram.Spectrogram.ResponseReceiver;
import com.loopj.android.http.RequestParams;

public class ProcessService extends Service {
	private int				fs;
	private int 			channelConfiguration;
	private int 			audioEncoding;
	
	private int 			blockSize = 256; 
	private int				freqSize = blockSize/2;
	
	private String 			targetURI = null;
	private String			db = null;
	private String			dev = null;
	private String			pwd = null;
	private String 			android_id = null; 
	private int				thresh;
	private int 			datSrc;
	private boolean			isSend;
	private boolean			isCollect;
	private boolean			isEvent;
	
	private Looper			mAsyncHttpLooper;
	private Looper 			mServiceLooper;

	private Context			mContext;
	private BatteryLevelMonitor mBatteryLevelMonitor;
	private CommunicationManager mCommunicationManager;
	private LocationData locationListener; // location variable declaration, amended by Duc, Aug,28 
	private AudioRecord audioRecord;
	 
	// Handler that receives messages from the thread
	private final class ServiceHandler extends Handler {
		Date currDate = null;
		
		public ServiceHandler(Looper looper) {
	        super(looper);
	    }
		
		@Override
		public void handleMessage(Message msg) {
			if (datSrc == 0){ //mic
				fs = 16000;
				channelConfiguration = AudioFormat.CHANNEL_IN_MONO;
				audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
				audioRecord = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
					fs,
					channelConfiguration,
					audioEncoding,
					AudioRecord.getMinBufferSize(fs, channelConfiguration, audioEncoding));
			}else{ // bluetooth
				fs = 6800;
				channelConfiguration = AudioFormat.CHANNEL_IN_MONO;
				audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
				audioRecord = new BtAudioRecord(MediaRecorder.AudioSource.DEFAULT,
					fs,
					channelConfiguration,
					audioEncoding,
					AudioRecord.getMinBufferSize(fs, channelConfiguration, audioEncoding),
					mContext);
			}
			
			short[] buffer;
			CircularFifoQueue<Short> winBuffer = new CircularFifoQueue<Short>(blockSize);
			ByteArrayOutputStream baos = null;
	
			try{
				audioRecord.startRecording();
			}
			catch(IllegalStateException e) {
				Toast mToast = Toast.makeText(mContext, "Failed to start recording", Toast.LENGTH_SHORT);
				mToast.setGravity(Gravity.TOP, 0, 0);
				TextView v = (TextView) mToast.getView().findViewById(android.R.id.message);
				v.setTextColor(Color.RED);
				mToast.show();
				
				GlobalState.getGlobalState().setBGstate(false);
			}
			
			int count = -1;
			RidgeTracker ridgetracker = null;
			CircularFifoQueue<byte[]> prevBuffer = null;
			if (isEvent){
				ridgetracker = new RidgeTracker(fs, blockSize, 0.1, thresh);
				prevBuffer = new CircularFifoQueue<byte[]>(ridgetracker.backtracklen);
			}else{
				count = 0;
			}
			while( GlobalState.getGlobalState().getBGstate() ) {
				// 50% overlapping
				buffer = new short[blockSize/2];
				// read in audio data
				audioRecord.read(buffer, 0, blockSize/2);
				for (int k=0;k<buffer.length;k++){
					winBuffer.add(buffer[k]);
				}
				
				// to the frequency domain
				short[] inFrame = new short[winBuffer.size()];
				for (int k=0;k<winBuffer.size();k++){
					inFrame[k] = winBuffer.get(k).shortValue();
				}
				double[] outFrame = new double[freqSize];
				process(inFrame, outFrame, freqSize);
				
				if (isEvent){
					// frequency domain processing
					ridgetracker.update(outFrame);
				}
				
				// New guiFrame
				GUIFrame guiFrame = new GUIFrame(outFrame);
				
				// Start and keep recording
				byte[] buffer2 = new byte[buffer.length * 2];
				ByteBuffer.wrap(buffer2).order(ByteOrder.nativeOrder()).asShortBuffer().put(buffer);
				if (isEvent){
					if (ridgetracker.ridgect > 0){
						try {
							if (baos == null){
								baos = new ByteArrayOutputStream();
								for (int k = 0; k < prevBuffer.size(); k++){
									baos.write(prevBuffer.get(k));
								}
							}
							baos.write(buffer2);
			            } catch (IOException e) {
			                    e.printStackTrace();
			            }
					} else{
						prevBuffer.add(buffer2);
					}
				} else{
					count = count + 1;
					try {
						if (baos == null){
							baos = new ByteArrayOutputStream();
						}
						baos.write(buffer2);
		            } catch (IOException e) {
	                    e.printStackTrace();
		            }
				}
				
				// Control wifi status
				if (isEvent){
					mCommunicationManager.update(ridgetracker.ridgect > 0);
				}else{
					mCommunicationManager.update(true);
				}
				
				// check for end of track
				boolean isEnd;
				if (isEvent){
					isEnd = ridgetracker.isReady;
				} else{
					isEnd = count >= 4000;
				}
				if (isEnd){
					if (!isEvent){
						count = 0;
					}
					// Sample time
					currDate = new Date();
					
					if (isSend){
						// *** send wave files
						RequestParams rpPostGrid = new RequestParams();
						rpPostGrid.put("dbname", db);
						if (isEvent){
							rpPostGrid.put("colname", "data");
						} else{
							rpPostGrid.put("colname", "data2");
						}
						rpPostGrid.put("user", dev);
						rpPostGrid.put("passwd", pwd);
						SimpleDateFormat fnf = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.US); // Locale.US for machine readability
						fnf.setTimeZone(TimeZone.getTimeZone("UTC")); // otherwise, SimpleDateFormat uses a default system timezone
						String fn = fnf.format(currDate);
						rpPostGrid.put("filename", fn+".wav"); // name file using time stamp
						WaveFile wavfile = new WaveFile((byte)16, fs, baos.size());
						byte[] wavebb = wavfile.toWaveByteBuffer(baos.toByteArray());
						new CommTask(mAsyncHttpLooper,getBaseContext(),mCommunicationManager,targetURI,rpPostGrid,wavebb).execute();
						
						// *** send metadata
						RequestParams rpPost = new RequestParams();
						rpPost.put("dbname", db);
						if (isEvent){
							rpPost.put("colname", "event");
						} else{
							rpPost.put("colname", "event2");
						}
						rpPost.put("user", dev);
						rpPost.put("passwd", pwd);
						JSONObject postObj;
						if (isEvent){
							postObj = prepareJSON(ridgetracker, fn);
						}else{
							postObj = prepareJSON(fn);
						}
						new CommTask(mAsyncHttpLooper,getBaseContext(),mCommunicationManager,targetURI,rpPost,postObj).execute();
					}
					
					// stop recording
					try {
                		if (baos != null){
                			baos.close();
                			baos = null;
                		}
		            } catch (IOException e) {
		                    e.printStackTrace();
		            }
					
					if (isEvent){
						// update gui frame and cleanup
						guiFrame.update(ridgetracker);
						ridgetracker.clearVolatileState();
					}
				}

				// update screen with processed results
				if( GlobalState.getGlobalState().getGUIstate() ){
					Intent broadcastIntent = new Intent();
					broadcastIntent.setAction(ResponseReceiver.ACTION_RESP);
					broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
					broadcastIntent.putExtra("GUIFrame", guiFrame);
					sendBroadcast(broadcastIntent);
				}
			}
			
			// Stop the service using the startId, so that we don't stop
			// the service in the middle of handling another job
			stopSelf(msg.arg1);
	    }
		
		private JSONObject prepareJSON(RidgeTracker ridgetracker, String fn){
			// =====
			JSONArray noiseFloorJson = new JSONArray();
			for (int i=0;i<ridgetracker.noiseFloor.length;i++){
				try {
					noiseFloorJson.put(ridgetracker.noiseFloor[i]);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			// =====
			JSONArray featJson = new JSONArray();
			for (int i=0;i<ridgetracker.feat.length;i++){
				try {
					featJson.put(ridgetracker.feat[i]);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			// =====
			JSONObject locationJson = new JSONObject();
			try{
				locationJson.put("type", "Point");
				JSONArray coord = new JSONArray();
				coord.put(locationListener.getLongtitude());
				coord.put(locationListener.getLatitude());
				locationJson.put("coordinates",  coord);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			// =====
			JSONObject recordDateJson = new JSONObject();
			try{
				SimpleDateFormat tsf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US); // ISO8601 uses trailing Z
				tsf.setTimeZone(TimeZone.getTimeZone("UTC"));
				String ts = tsf.format(currDate);
				recordDateJson.put("$date", ts);
			} catch (JSONException e){
				e.printStackTrace();
			}
			// ===== for debugging
			JSONArray FIJson = new JSONArray();
			JSONArray TIJson = new JSONArray();
			//JSONArray MJson = new JSONArray();
			//JSONArray groupIdJson = new JSONArray();
			if (isCollect){
				for (int i=0;i<ridgetracker.FI.size();i++){
					FIJson.put(ridgetracker.FI.get(i));
					TIJson.put(ridgetracker.TI.get(i));
					//MJson.put(ridgetracker.M.get(i));
					//groupIdJson.put(ridgetracker.groupId.get(i));
				}
			}
	        
			// putting it together
			JSONObject json = new JSONObject();
			try{
				json.put("androidID", android_id);
				json.put("serviceAddr", new URL(targetURI).getHost());
				json.put("filename", fn+".wav");
				json.put("fs", fs);
				json.put("fftSize", blockSize);
				json.put("batteryLevel", mBatteryLevelMonitor.getBatteryLevel());
				json.put("noiseFloor", noiseFloorJson);
				json.put("feat", featJson);
				json.put("recordDate", recordDateJson);
				json.put("location", locationJson);
				// for debugging
				if (isCollect){ 
					json.put("FI", FIJson);
					json.put("TI", TIJson);
					//json.put("M", MJson);
					//json.put("groupId", groupIdJson);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (MalformedURLException e){
				e.printStackTrace();
			}
			
			return json;
		}
		
		private JSONObject prepareJSON(String fn){
			// =====
			JSONObject locationJson = new JSONObject();
			try{
				locationJson.put("type", "Point");
				JSONArray coord = new JSONArray();
				coord.put(locationListener.getLongtitude());
				coord.put(locationListener.getLatitude());
				locationJson.put("coordinates",  coord);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			// =====
			JSONObject recordDateJson = new JSONObject();
			try{
				SimpleDateFormat tsf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US); // ISO8601 uses trailing Z
				tsf.setTimeZone(TimeZone.getTimeZone("UTC"));
				String ts = tsf.format(currDate);
				recordDateJson.put("$date", ts);
			} catch (JSONException e){
				e.printStackTrace();
			}
			
			// putting it together
			JSONObject json = new JSONObject();
			try{
				json.put("androidID", android_id);
				json.put("serviceAddr", new URL(targetURI).getHost());
				json.put("filename", fn+".wav");
				json.put("fs", fs);
				json.put("fftSize", blockSize);
				json.put("batteryLevel", mBatteryLevelMonitor.getBatteryLevel());
				json.put("recordDate", recordDateJson);
				json.put("location", locationJson);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (MalformedURLException e){
				e.printStackTrace();
			}
			
			return json;
		}
	}
	
	@Override
	public void onCreate() {
		// Start up the thread running the service.  Note that we create a
	    // separate thread because the service normally runs in the process's
	    // main thread, which we don't want to block.  We also make it
	    // background priority so CPU-intensive work will not disrupt our UI.
	    
		HandlerThread thread = new HandlerThread("ServiceHandler",
	    		android.os.Process.THREAD_PRIORITY_BACKGROUND);
	    thread.start();
	    
	    // Get the HandlerThread's Looper and use it for our Handler
	    mServiceLooper = thread.getLooper();
	    
	    HandlerThread thread2 = new HandlerThread("AsyncHttpResponseHandler",
	    		android.os.Process.THREAD_PRIORITY_BACKGROUND);
	    thread2.start();
	    
	    mAsyncHttpLooper = thread2.getLooper();
	    
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mContext = this;
		Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
		Log.i("ProcessService", "service starting");
		
		Bundle bundle = intent.getBundleExtra("Menu.Bundle");
		targetURI = bundle.getString("serverURI");
		dev = bundle.getString("devName");
		pwd = bundle.getString("password");
		db = bundle.getString("database");
		android_id = bundle.getString("androidID");
		thresh = bundle.getInt("thresh");
		datSrc = bundle.getInt("datSrc");
		isCollect = bundle.getBoolean("isCollect");
		isSend = bundle.getBoolean("isSend");
		isEvent = bundle.getBoolean("isEvent");
		
		// init communication manager
		mCommunicationManager = new CommunicationManager(this);
		// init battery level monitor
		mBatteryLevelMonitor = new BatteryLevelMonitor(this);
		//initialize the locationListener
		locationListener = new LocationData(this);
		
		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the job
		ServiceHandler serviceHandler = new ServiceHandler(mServiceLooper);
		Message msg = serviceHandler.obtainMessage();
		msg.arg1 = startId;
		serviceHandler.sendMessage(msg);
		
		// We want this service to continue running until it is explicitly
	    // stopped, so return sticky.
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
	    // We don't provide binding, so return null
	    return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		locationListener.stopLocationUpdate();
		try{
			audioRecord.stop();
			audioRecord.release();
		}
		catch(IllegalStateException e) {
			Log.e("ProcessService", e.toString());
		}
		Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
		Log.i("ProcessService", "service done");
	}
	
	public static native void process(short[] inbuf, double[] outbuf, int N);

}
