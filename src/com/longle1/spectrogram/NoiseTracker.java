/*******************************************************************************
 * Copyright (c) 2014 University of Illinois at Urbana-Champaign.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Long Le, David Jun, and Douglas Jones - initial API and implementation
 *******************************************************************************/
package com.longle1.spectrogram;

public class NoiseTracker {
	final double alpha_updown;
	final double slow_scale;
	final int indicator_countdown;
	final double[] floor_up;
	final double[] floor_up_slow;
	final double[] floor_down;
	double[] floor_thresh;
	int[] indicator_last;
	
	public NoiseTracker(int nBlk, int nFc, double fs, double btLen){
		alpha_updown = 0.01;
		slow_scale = 0.5;
		indicator_countdown = (int)Math.ceil(btLen*fs/(nBlk/2));
		floor_up = new double[nFc];
		for (int i = 0; i < nFc; i++){
			floor_up[i] = 1+alpha_updown;
		}
		floor_up_slow = new double[nFc];
		for (int i = 0; i < nFc; i++){
			floor_up_slow[i] = 1+slow_scale*alpha_updown;
		}
		floor_down = new double[nFc];
		for (int i = 0; i < nFc; i++){
			floor_down[i] = 1-alpha_updown;
		}
		
		floor_thresh = new double[nFc];
		for (int i = 0; i < nFc; i++){
			floor_thresh[i] = 0.01;
		}
		indicator_last = new int[nFc]; // default to 0
	}
	
	public void update(double[] frame, int j){
		if (frame[j] > floor_thresh[j]){
			indicator_last[j] = indicator_last[j] - 1;
			if (indicator_last[j] <= 0){ // signal is highly probable, slow down
				floor_thresh[j] = Math.max(1e-6, floor_thresh[j]*floor_up_slow[j]);
			}
			else{ // noise is still highly probable
				floor_thresh[j] = Math.max(1e-6, floor_thresh[j]*floor_up[j]);
			}
		}
		else{ // noise is highly probable
	        floor_thresh[j] = Math.max(1e-6, floor_thresh[j]*floor_down[j]);
	        indicator_last[j] = indicator_countdown;
		}
	}
}
