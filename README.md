# README #

NOTE: AS OF 2015-11-19, THIS REPO IS OBSOLETE!!! THE PROJECT HAS BEEN MIGRATED TO ANDROID STUDIO IN THE NEW [REPO](https://bitbucket.org/longle1/sas-sensor).

This Android app is the client component of the [Illiad](https://acoustic.ifp.illinois.edu) service. The app performs anomalous acoustic event detection by ridge tracking in the short-time Fourier transform. 

### Description ###

* This repo contains both the source code and the binary for the Illiad client
* [The paper describing the ridge tracking technique will be made available soon](https://sites.google.com/site/longle1illinois/publications)

### Usage ###

* The binary (*.apk) can be readily downloaded under the bin/ folder, and the rest are source codes. Building from source requires the following dependencies
* Dependencies: commons-collections, commons-lang, commons-math at [here](http://commons.apache.org/proper/commons-collections/), and [android-async-http](http://loopj.com/android-async-http/)
* Instructions: Once the app starts, the spectrogram (squared magnitude of the short-time Fourier transform) can be enabled by checking the "Enable Spectrogram" box.

### Contact ###

* Any feedback are highly appreciated, so please sent them to longle1 at illinois dot edu